# Image classification demo
## Intro
Image classification demo for classify images of streets, parks, lakes, etc. from osm-extractor.

## Usage
./run.sh [images directory] [img_height] [img_width] <br>
The [images directory] has multiple subdirectories with name as tag such as below.
```
.
├── Streets
│   ├── 0.jpg
│   ├── 1.jpg
│   ├── ...
└── Parks
│   ├── 0.jpg
│   ├── 1.jpg
│   ├── ...
└── Lakes
    ├── 0.jpg
    ├── 1.jpg
    └── ...
```
